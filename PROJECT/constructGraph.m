function constructGraph
center = 2;
left = 1;
right = 3;
[matrix,row,columns] = readFile();

graph = [8 8];
for i=1:8
    for j=1:8
        graph(i,j) = 0;
    end
end

for i=1:row
    
    centerPos = matrix(i,center);
    leftPos = matrix(i,left);
    rightPos = matrix(i,right);
    
    fprintf('%d %d %d\n',leftPos,centerPos,rightPos);
    
    graph(centerPos,leftPos) = 1;
    graph(centerPos,rightPos) = 1;
    if (i>1)
        
        centerAnt = matrix(i-1,center);
        leftAnt = matrix(i-1,left);
        rightAnt = matrix(i-1,right);
        
        if(centerAnt ~= centerPos)
            graph(centerPos,centerAnt) = 1;
        end
        if(leftAnt ~= leftPos)
            graph(leftPos,leftAnt) = 1;
        end
        if(rightAnt ~= rightPos)
            graph(rightPos,rightAnt) = 1;
        end
        
        
    end
end

graph

end