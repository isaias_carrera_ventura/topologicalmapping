function testLocation

%reference values
left = 1;
center = 2;
right = 3;

%Get the video segmentation
[matrix,row,~] = segmentationSequence(); 

%Declaration graphFinal matrix video segmentation and array usable for algorithm
graphFinal = [6 6];
org = (3);
aux = (3);

%Initi variables
initgraphFinal
initArrayAux

%init variables
countNode = 1;
leftPosAct = 0;
centerPosAct = 0;
rightPosAct = 0;
leftPosAnt = 0;
centerPosAnt = 0;
rightPosAnt = 0;

centerCountRepetitive = 0;
rightCountRepetitive = 0;
leftCountRepetitive = 0;

limitCount = 20;

cnt = 1;
times = 1;
band = 0;

for i=1:row
    
    leftPosAct = matrix(i,left);
    centerPosAct = matrix(i,center);
    rightPosAct = matrix(i,right);
    
    if(cnt == 50 && band == 0)
        
        p = plot(graph(graphFinal));
        p.Marker = 'diamond';
        p.NodeColor = 'r';
        
        save('graphAuxGenerated.mat','graphFinal');
        band = 1;
        
        figure()
        cnt = 1;
        
        graphFinal = [6 6];
        org = (3);
        aux = (3);

        %Initi variables
        initgraphFinal
        initArrayAux

        %init variables
        countNode = 1;
        
    else
        
        if cnt == 1
            InitialNode
        else
            leftPosAnt = matrix(i-1,left);
            centerPosAnt = matrix(i-1,center);
            rightPosAnt = matrix(i-1,right);

            %check wheter segmentation changed in left, right o center.
            if leftPosAnt ~= leftPosAct 
                countNode = countNode + 1;
                aux(left) = countNode;
            else
                leftCountRepetitive = leftCountRepetitive + 1;
                if(leftCountRepetitive == limitCount)
                    countNode = countNode +1;
                    aux(left) = countNode;
                    leftCountRepetitive = 0;
                else
                    aux(left) = org(left);
                end
            end

            if centerPosAnt ~= centerPosAct 
                countNode = countNode + 1;
                aux(center) = countNode;
            else
                centerCountRepetitive = centerCountRepetitive + 1;
                if(centerCountRepetitive == limitCount)
                    countNode = countNode + 1;
                    aux(center) = countNode;
                    centerCountRepetitive = 0;

                else
                    aux(center) = org(center);
                end
            end

            if rightPosAnt ~= rightPosAct 
                countNode = countNode + 1;
                aux(right) = countNode;
            else
                rightCountRepetitive = rightCountRepetitive + 1;
                if(rightCountRepetitive == limitCount)
                    countNode = countNode + 1;
                    aux(right) = countNode;
                    rightCountRepetitive = 0;
                else
                    aux(right) = org(right);
                end
            end

            %Check if we adjacent the nodes
            if org(left) ~= aux(left)
                fprintf('%d -> %d\n',org(left),aux(left));
                graphFinal(org(left),aux(left)) = 1;
                graphFinal(aux(left),org(left)) = 1;
            end

            if org(center) ~= aux(center)
                fprintf('%d -> %d\n',org(center),aux(center));
                graphFinal(org(center),aux(center)) = 1;
                graphFinal(aux(center),org(center)) = 1;
            end

            if org(right) ~= aux(right)
                fprintf('%d -> %d\n',org(right),aux(right));
                graphFinal(org(right),aux(right)) = 1;
                graphFinal(aux(right),org(right)) = 1;
            end


            org(left) = aux(left);
            org(center) = aux(center);
            org(right) = aux(right);

            adjacentNodesY
        end
        
        
        cnt = cnt + 1;
        
       
    end
    
    
    
    
end



%%

    function InitialNode
        addNodeToArray(countNode,left)
        countNode = countNode + 1;
        addNodeToArray(countNode,center)
        countNode = countNode + 1;
        addNodeToArray(countNode,right)
        adjacentNodesY
    end

    function addNodeToArray(cN,position) 
        org(position) = cN;
    end

    function printgraphFinal
        fprintf('\n');
        for f=1:countNode
            for c=1:countNode
                fprintf('%d',graphFinal(f,c));
            end
            fprintf('\n');
        end
        
    end

   

    function adjacentNodesY
    
       for i1=1:2
           nodeX= org(i1);
           nodeY = org(i1 + 1);
           graphFinal(nodeX,nodeY) = 1;
           graphFinal(nodeY,nodeX) = 1;
       end
        
    end

    function initgraphFinal  
        for countI=1:6;
            for countJ=1:6;
                graphFinal(countI,countJ) = 0;
            end
        end
    end

    function initArrayAux
        for init=1:3;
            org(init) = 0;
            aux(init) = 0;
        end
    end

end
