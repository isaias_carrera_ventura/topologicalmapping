function position = compareVectorNode(histogram, nodeVector,counter)%,originPosition)

    position = -1;
    [~,row] = size(nodeVector);
    %fprintf('position to search %d',originPosition)
    for j=1:row
        if getSumFromFrames(j) < 20.0
            fprintf('times %d :',counter); 
            fprintf('Position : %d %.2f',j,getSumFromFrames(j));
            position = j;
        end
    end
    
    fprintf('\n');

    function sum = getSumFromFrames(position)
        sum = 0;
        for x = 1:8
            sum = sum + pow2(nodeVector(x,position)-histogram(x));
        end        
        %fprintf('sum n: %f\t',sum);
        %sum = 100 - sum;
        %fprintf('sum w: %f\n',sum);
    end

end

