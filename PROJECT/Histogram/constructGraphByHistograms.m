function constructGraphByHistograms()

[matrix,row,~] = segmentationSequenceComplete(); 

countToProcessProm = 1;
countBlocks = 0;
limitCounter = 15;
mVector = [1 8];
vectorAux = [1 8];
nodeCounterX = 1;
nodeCounterY = 2;
graphConstructed = zeros(1,1);

    function cleanVectorAux() 
        for z=1:8
            vectorAux(1,z) = 0;
        end
    end

cleanVectorAux();

for i=1:row
   
    if(countToProcessProm == limitCounter)
        
        countToProcessProm = 0;
        countBlocks = countBlocks + 1;
        
        addElementAverageToVector(i,countBlocks);
        if(countBlocks>1)
            if (getSumFromFrames(countBlocks,countBlocks-1) == 1)
                pause(1);
                G = graph(graphConstructed);
                plot(G);
            end
        end
        
    end
    countToProcessProm = countToProcessProm + 1;
 
    
end


    function addElementAverageToVector(posLimitInSequence,countBlocks)
        
        for f=(posLimitInSequence-limitCounter+1):posLimitInSequence
            for c=1:8
                %fprintf('- %.3f',matrix(f,c));
                vectorAux(1,c) = vectorAux(1,c) + matrix(f,c);
            end
        end
        
        %Average
        for x=1:8
            mVector(countBlocks,x) = vectorAux(1,x)/limitCounter;
        end
        cleanVectorAux();
        
    end

    
    function bandNodeCreated = getSumFromFrames(current,last)
        sum = 0;
        for x=1:8
            sum = sum + pow2(mVector(current,x)-mVector(last,x));
        end
        sum = 100 - sum;clc
        if(sum < 85.0)
            fprintf('--------------add node------x: %d y %d\n',nodeCounterX,nodeCounterY);
            graphConstructed(nodeCounterX,nodeCounterY) = 1;
            graphConstructed(nodeCounterY,nodeCounterX) = 1;
            nodeCounterX = nodeCounterX + 1;
            nodeCounterY = nodeCounterY + 1;
            bandNodeCreated = 1;
        else
            bandNodeCreated = 0;
        end
        
    end

end

