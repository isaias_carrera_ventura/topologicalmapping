function [matrixSequence,row,col] = segmentationSequenceComplete()


fid = fopen('preds_.txt','r');
formatSpec = '%f/%f/%f/%f/%f/%f/%f/%f';
sizeA = [8 inf];
A = fscanf(fid,formatSpec,sizeA);
%Define new matrix to process easier
[row, col] = size(A);
aux = row;
row = col;
col = aux;
matrixSequence = [row col];
for i=1:row
    for j=1:col
        matrixSequence(i,j) = A(j,i);
    end
end

end

