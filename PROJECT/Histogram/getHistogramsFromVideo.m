function getHistogramsFromVideo

clf

p = importdata('p.mat');
nvals  = 8;
rho= .5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
loss_spec = 'trunc_cl_trwpll_5';
crf_type  = 'linear_linear';
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};
feat_params = {{'patches',0},{'position',1},{'fourier',1},{'hog',8}};

% colormap-- taken from http://dags.stanford.edu/projects/scenedataset.html
cmap = [.5  .5  .5;
        .5  .5   0;
        .5  .25 .5;
        0   .5   0;
        0    0   .5;
        .5   0   0;
        .5  .31  0;
        1   .5   0];
    
clf
vid_read  = VideoReader('/home/isaiasr/Documents/MATLAB/img.m4v');
vid_write = VideoWriter('marginals'); open(vid_write);

fid = fopen('preds_.txt','wt');

for i=1:vid_read.NumberOfFrames,
   
    % load data, make features, etc.
    im0   = double(read(vid_read,i))/255;
    im0 = imresize(im0,0.5);
    feats = featurize_im(im0,feat_params);
    im    = imresize(im0,.2,'bilinear');
    feats = imresize(feats,.2,'bilinear');
    [ly lx lz] = size(feats);
    feats = reshape(feats,ly*lx,lz);
    if i==1
        [ly lx lz] = size(im);
        model = gridmodel(ly,lx,nvals);
    end
    efeats = edgeify_im(im,edge_params,model.pairs,model.pairtype);
    
    % do inference
    [b_i b_ij] = eval_crf(p,feats,efeats,model,loss_spec,crf_type,rho);
    
    % interpolate beliefs to original image, get predictions
    [ly0 lx0 lz] = size(im0);
    b_i = reshape(b_i',[ly lx nvals]);
    b_i = imresize(b_i,[ly0 lx0],'bilinear');
    [~,x_pred] = max(b_i,[],3);

    
    %GET PARAMS
    [height,width] = size(x_pred);
    numTotalPixels = height*width;
    ocurrencies = getHistogramPerFrame(1,width,height,x_pred);
   
    fprintf(fid,'%.3f/%.3f/%.3f/%.3f/%.3f/%.3f/%.3f/%.3f',(ocurrencies(1)/numTotalPixels)*100,(ocurrencies(2)/numTotalPixels)*100,(ocurrencies(3)/numTotalPixels)*100,(ocurrencies(4)/numTotalPixels)*100,(ocurrencies(5)/numTotalPixels)*100,(ocurrencies(6)/numTotalPixels)*100,(ocurrencies(7)/numTotalPixels)*100,(ocurrencies(8)/numTotalPixels)*100);
    fprintf(fid,'\n');
    
    %Print to console
    fprintf('1.-%.2f 2.-%.2f 3.-%.2f 4.-%.2f 5.-%.2f 6.-%.2f 7.-%.2f 8.-%.2f\n',(ocurrencies(1)/numTotalPixels)*100,(ocurrencies(2)/numTotalPixels)*100,(ocurrencies(3)/numTotalPixels)*100,(ocurrencies(4)/numTotalPixels)*100,(ocurrencies(5)/numTotalPixels)*100,(ocurrencies(6)/numTotalPixels)*100,(ocurrencies(7)/numTotalPixels)*100,(ocurrencies(8)/numTotalPixels)*100);

    im_gray = repmat(rgb2gray(im0),[1 1 3]);
    
    colormap(cmap)
    preds = miximshow(b_i,nvals);
    im_mix = .25*im_gray + .75*preds;
    imshow(im_mix)
    colorbar('Location','South','XTickLabel',...
    {'sky','tree','road','grass','water','bldg','mntn','fg obj'});

    % write output video
    currFrame = getframe;
    writeVideo(vid_write,currFrame);

    drawnow;
end

fclose(fid);
close(vid_write);




end

