function relocalization
clf
%http://chtech.co.za/wordpress/parrot-bebop-2-lens-repair/
p = importdata('p.mat');
nvals  = 8;
rho= .5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
loss_spec = 'trunc_cl_trwpll_5';
crf_type  = 'linear_linear';
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};
feat_params = {{'patches',0},{'position',1},{'fourier',1},{'hog',8}};

% colormap-- taken from http://dags.stanford.edu/projects/scenedataset.html
cmap = [.5  .5  .5;
        .5  .5   0;
        .5  .25 .5;
        0   .5   0;
        0    0   .5;
        .5   0   0;
        .5  .31  0;
        1   .5   0];

clf
%s = rossubscriber('/image_raw');
i=1;
%--------------------------
frameCounter = 1;
histogramSequence = [1 8];
countToProcessProm = 1;
countBlocks = 0;
limitCounter = 5;
mVector = [1 8];
graphConstructed = zeros(1,1);
vectorAux = [1 8];

    function cleanVectorAux() 
        for z=1:8
            vectorAux(1,z) = 0;
        end
    end

cleanVectorAux();
%--------- 
vectorNode = getVectorNode();
graphVector = getMatrixGraph();
vectorImageDisk = getArrayImage();
vid_read  = VideoReader('/home/isaiasr/Desktop/bebop_video/3/vuelta/vueltaresize.mpeg');


for i=1:vid_read.NumberOfFrames,

%     %Receive image from ROS
%     data = receive(s);
%     img = readImage(data);
%     im0 = double(img)/255;
%     im0 = imresize(im0,0.5);

    
    im0   = double(read(vid_read,i))/255;
    im0 = imresize(im0,0.5);

    feats = featurize_im(im0,feat_params);
    im    = imresize(im0,.2,'bilinear');
    feats = imresize(feats,.2,'bilinear');
    [ly lx lz] = size(feats);
    feats = reshape(feats,ly*lx,lz);
    if i==1
        [ly lx lz] = size(im);
        model = gridmodel(ly,lx,nvals);
        i = i + 1;
    end
    efeats = edgeify_im(im,edge_params,model.pairs,model.pairtype);
    
    % do inference
    [b_i b_ij] = eval_crf(p,feats,efeats,model,loss_spec,crf_type,rho);
    
    % interpolate beliefs to original image, get predictions
    [ly0 lx0 lz] = size(im0);
    b_i = reshape(b_i',[ly lx nvals]);
    b_i = imresize(b_i,[ly0 lx0],'bilinear');
    [~,x_pred] = max(b_i,[],3);
    
    %-------------------------------------------------
    %PROCESS CURRENT FRAME ALGORITHM 
    %GET PARAMS
    [height,width] = size(x_pred);
    numTotalPixels = height*width;
    ocurrencies = getHistogramPerFrame(1,width,height,x_pred);
    %Print to console sequence histogram
    %fprintf('1.-%.2f 2.-%.2f 3.-%.2f 4.-%.2f 5.-%.2f 6.-%.2f 7.-%.2f 8.-%.2f\n',(ocurrencies(1)/numTotalPixels)*100,(ocurrencies(2)/numTotalPixels)*100,(ocurrencies(3)/numTotalPixels)*100,(ocurrencies(4)/numTotalPixels)*100,(ocurrencies(5)/numTotalPixels)*100,(ocurrencies(6)/numTotalPixels)*100,(ocurrencies(7)/numTotalPixels)*100,(ocurrencies(8)/numTotalPixels)*100);

    for index=1:8
        histogramSequence(frameCounter,index) = (ocurrencies(index)/numTotalPixels)*100;
    end
    
    if(countToProcessProm == limitCounter)
        
        countToProcessProm = 0;
        countBlocks = countBlocks + 1;
        addElementAverageToVector(frameCounter,countBlocks);
        arrayFrame = [];
        for i=1:8
            arrayFrame(i) = mVector(countBlocks,i);
        end
        fprintf('\n');
        
        position = compareVectorNode(arrayFrame,vectorNode,countBlocks);
        subplot(2,1,2);
        G = graph(graphVector);
        if position ~= -1
            hLine = plot(G);
            highlight(hLine,[position],'NodeColor','g');
            view(270, 100);
            axes('pos',[.5 .5 .6 .5])
            fileName = vectorImageDisk(:,position);
            fileNameStr = cell2mat(fileName);
            imgLocation = imread(fileNameStr); 
            imshow(imgLocation);
            pause(3);
           
        end
    end
    
    countToProcessProm = countToProcessProm + 1;    
    frameCounter = frameCounter + 1;
    
    
    %-------------------------------------------------
    im_gray = repmat(rgb2gray(im0),[1 1 3]);
    colormap(cmap)
    preds = miximshow(b_i,nvals);
    im_mix = .25*im_gray + .75*preds;
    axes('pos',[0 .6 .6 .4])
    imshow(im_mix)

end

    function addElementAverageToVector(posLimitInSequence,countBlocks)
        
        for f=(posLimitInSequence-limitCounter+1):posLimitInSequence
            for c=1:8
                vectorAux(1,c) = vectorAux(1,c) + histogramSequence(f,c);
                %fprintf('%f  ',histogramSequence(f,c));
            end
            %fprintf('\n');
        end
        %Average
        for x=1:8
            mVector(countBlocks,x) = vectorAux(1,x)/limitCounter;
        end
        cleanVectorAux();
        
    end

end