function matrixNode = getMatrixGraph( )

nodeVector = getVectorNode();
[~,nodeNumber] = size(nodeVector);

matrixNode = [nodeNumber,nodeNumber];
for i=1:nodeNumber
    for j=1:nodeNumber
        matrixNode(i,j)=0;
    end
end

for i=1:nodeNumber
    matrixNode(i,i+1) = 1;
    matrixNode(i+1,i) = 1;
end

% g = graph(matrixNode);
% c = plot(g);
% highlight(c,[1],'NodeColor','g');


end

