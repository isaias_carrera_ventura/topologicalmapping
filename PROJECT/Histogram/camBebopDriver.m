function camBebopDriver 
clf
%http://chtech.co.za/wordpress/parrot-bebop-2-lens-repair/
p = importdata('p.mat');
nvals  = 8;
rho= .5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
loss_spec = 'trunc_cl_trwpll_5';
crf_type  = 'linear_linear';
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};
feat_params = {{'patches',0},{'position',1},{'fourier',1},{'hog',8}};

% colormap-- taken from http://dags.stanford.edu/projects/scenedataset.html
cmap = [.5  .5  .5;
        .5  .5   0;
        .5  .25 .5;
        0   .5   0;
        0    0   .5;
        .5   0   0;
        .5  .31  0;
        1   .5   0];

clf
% s = rossubscriber('/image_raw');
i=1;
%--------------------------
frameCounter = 1;
histogramSequence = [1 8];
countToProcessProm = 1;
countBlocks = 0;
limitCounter = 5;
mVector = [1 8];
nodeCounterX = 1;
nodeCounterY = 2;
graphConstructed = zeros(1,1);
vectorAux = [1 8];

    function cleanVectorAux() 
        for z=1:8
            vectorAux(1,z) = 0;
        end
    end

cleanVectorAux();
%---------

bandImageNode = 0;
fid = fopen('preds_.txt','wt'); %TEXT TO SAVE HISTOGRAMS NODES
fidImageArray = fopen('imageNameArray.txt','wt');
counterImageSaveInFile = 0;
vid_read  = VideoReader('/home/isaiasr/Desktop/bebop_video/3/ida/idaresize.mpeg');


for i=1:vid_read.NumberOfFrames,
    
%while(1)
    
%     %Receive image from ROS 
%     data = receive(s);
%     img = readImage(data);
%     im0 = double(img)/255;
%     im0 = imresize(im0,0.5);

    im0   = double(read(vid_read,i))/255;
    im0 = imresize(im0,0.5);

    
    feats = featurize_im(im0,feat_params);
    im    = imresize(im0,.2,'bilinear');
    feats = imresize(feats,.2,'bilinear');
    [ly lx lz] = size(feats);
    feats = reshape(feats,ly*lx,lz);
    if i==1
        [ly lx lz] = size(im);
        model = gridmodel(ly,lx,nvals);
        i = i + 1;
    end
    efeats = edgeify_im(im,edge_params,model.pairs,model.pairtype);
    
    % do inference
    [b_i b_ij] = eval_crf(p,feats,efeats,model,loss_spec,crf_type,rho);
    
    % interpolate beliefs to original image, get predictions
    [ly0 lx0 lz] = size(im0);
    b_i = reshape(b_i',[ly lx nvals]);
    b_i = imresize(b_i,[ly0 lx0],'bilinear');
    [~,x_pred] = max(b_i,[],3);
    
    %-------------------------------------------------
    %PROCESS CURRENT FRAME ALGORITHM 
    %GET PARAMS
    [height,width] = size(x_pred);
    numTotalPixels = height*width;
    ocurrencies = getHistogramPerFrame(1,width,height,x_pred);
    %Print to console sequence histogram
    fprintf('1.-%.2f 2.-%.2f 3.-%.2f 4.-%.2f 5.-%.2f 6.-%.2f 7.-%.2f 8.-%.2f\n',(ocurrencies(1)/numTotalPixels)*100,(ocurrencies(2)/numTotalPixels)*100,(ocurrencies(3)/numTotalPixels)*100,(ocurrencies(4)/numTotalPixels)*100,(ocurrencies(5)/numTotalPixels)*100,(ocurrencies(6)/numTotalPixels)*100,(ocurrencies(7)/numTotalPixels)*100,(ocurrencies(8)/numTotalPixels)*100);

    for index=1:8
        histogramSequence(frameCounter,index) = (ocurrencies(index)/numTotalPixels)*100;
    end
    
    if(countToProcessProm == limitCounter)
        
        countToProcessProm = 0;
        countBlocks = countBlocks + 1;
        
        addElementAverageToVector(frameCounter,countBlocks);
        
        if(countBlocks>1)
            if (getSumFromFrames(countBlocks,countBlocks-1) == 1)
                
                %view(90,0)  % YZ
                subplot(2,1,2);
                G = graph(graphConstructed);
                
                plot(G);
                
                fprintf('node created\n');
                fprintf('Histogram\n');
                
                fprintf(fid,'%f/%f/%f/%f/%f/%f/%f/%f',mVector(countBlocks,1),mVector(countBlocks,2),mVector(countBlocks,3),mVector(countBlocks,4),mVector(countBlocks,5),mVector(countBlocks,6),mVector(countBlocks,7),mVector(countBlocks,8));
    fprintf(fid,'\n');
    
                
                bandImageNode = 1;
            end
        end
        
    end
    
    countToProcessProm = countToProcessProm + 1;    
    frameCounter = frameCounter + 1;
    
    
    %-------------------------------------------------
    im_gray = repmat(rgb2gray(im0),[1 1 3]);
    colormap(cmap)
    preds = miximshow(b_i,nvals);
    im_mix = .25*im_gray + .75*preds;
    axes('pos',[0 .6 .6 .4])
    imshow(im_mix)
    %colorbar('Location','South','XTickLabel',...
    %{'sky','tree','road','grass','water','bldg','mntn','fg obj'});

    if (bandImageNode==1)
        
        bandImageNode=0;
        axes('pos',[.6 .6 .5 .3])
        imshow(im_mix)
        counterImageSaveInFile = counterImageSaveInFile + 1
        fileName = strcat('/home/isaiasr/Documents/MATLAB/MATLAB/PROJECT/imageVideo/img_',int2str(counterImageSaveInFile),'.png');
        fprintf(fidImageArray,'%s',fileName);
        fprintf(fidImageArray,'\n');
        imwrite(im0,fileName);
        
    end
    
end

    function addElementAverageToVector(posLimitInSequence,countBlocks)
        
        for f=(posLimitInSequence-limitCounter+1):posLimitInSequence
            for c=1:8
                vectorAux(1,c) = vectorAux(1,c) + histogramSequence(f,c);
                
            end
        end
        
        %Average
        for x=1:8
            mVector(countBlocks,x) = vectorAux(1,x)/limitCounter;
        end
        cleanVectorAux();
        
    end

    function bandNodeCreated = getSumFromFrames(current,last)
        sum = 0;
        for x=1:8
            sum = sum + pow2(mVector(current,x)-mVector(last,x));
        end
        fprintf('1.- %f \n',sum);
        %sum = 100 - sum;
        %fprintf('2.- %f \n',sum);
        %if(sum < 85.0)
        if(sum > 50.0)
           graphConstructed(nodeCounterX,nodeCounterY) = 1;
           graphConstructed(nodeCounterY,nodeCounterX) = 1;
           nodeCounterX = nodeCounterX + 1;
           nodeCounterY = nodeCounterY + 1;
           bandNodeCreated = 1;
        else
           bandNodeCreated = 0;
        end
    end

end

