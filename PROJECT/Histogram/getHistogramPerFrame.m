function [bOcurrencies] = getHistogramPerFrame(posinit,finalpos,height,matrix )


bOcurrencies = [0,0,0,0,0,0,0,0];

for i = 1:height
    for j = posinit:finalpos
        bOcurrencies(matrix(i,j)) = bOcurrencies(matrix(i,j)) + 1;
    end
end


end