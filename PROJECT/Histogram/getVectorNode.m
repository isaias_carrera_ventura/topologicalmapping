function nodeVector = getVectorNode( )

fid = fopen('preds_.txt','r');
formatSpec = '%f/%f/%f/%f/%f/%f/%f/%f';
sizeA = [8 inf];
A = fscanf(fid,formatSpec,sizeA);
nodeVector = A;

end

