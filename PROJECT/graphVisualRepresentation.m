function graphVisualRepresentation

% Clear the workspace and the screen
sca;
close all;
clearvars;
% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);
screens = Screen('Screens');
screenNumber = max(screens);
black = BlackIndex(screenNumber);
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, black);
%[screenXpixels, screenYpixels] = Screen('WindowSize', window);
%[xCenter, yCenter] = RectCenter(windowRect);
baseRect = [0 0 20 20];
%centeredRect = CenterRectOnPointd(baseRect, 10, 100);
rectColor = [1 0 0];
lineColor = [0.5 0.5 0.5];
lineWidth = 1;

% PARAMS FOR ALGORITHM
 center = 2;
 left = 1;
 right = 3;
 [matrix,row] = readFile();
 
 x = 1;
 y = 2;
 p1 = [10 400];
 p2 = [10 500];
 p3 = [10 600];
 inc = 50;
 
 bandLeft = false;
 bandRight = false;
 
 
 for i=1:row
     
    centerPos = matrix(i,center);
    leftPos = matrix(i,left);
    rightPos = matrix(i,right);
    
    if(i>1)
        
        %LEFTNODE
        if(leftPos ~= matrix(i-1,left))
            createNodeLeft(leftPos)
        end
        
        %CENTERNODE
        if(centerPos ~= matrix(i-1,center))
            createNodeCenter(centerPos)
        end
        
        %RIGHTNODE
        if(rightPos ~= matrix(i-1,right))
            createNodeRight(rightPos)
        end
        
        %ADJACENT
        Screen('DrawLine', window, lineColor, p1(x), p1(y), p2(x), p2(y),lineWidth);
        Screen('DrawLine', window, lineColor, p3(x), p3(y), p2(x), p2(y),lineWidth);
        
    else
        createInitialNode
    end
 end
 
%DRAW IMAGE
Screen('Flip', window);
%KEY EVENT
KbStrokeWait;
sca;

    %FUNCTION TO CREATE INITIAL NODE
    function createInitialNode
       Screen('FillRect', window, rectColor, CenterRectOnPointd(baseRect, p1(x), p1(y)));
       Screen('FillRect', window, rectColor, CenterRectOnPointd(baseRect, p2(x), p2(y)));
       Screen('FillRect', window, rectColor, CenterRectOnPointd(baseRect, p3(x), p3(y)));
       %LINES
       Screen('DrawLine', window, lineColor, 10, 400, 10, 500,lineWidth);
       Screen('DrawLine', window, lineColor, 10, 500, 10, 600,lineWidth);
    end

    function createNodeLeft(label)
        
        color = getColor(label);
        
        posAnt = p1(x);
        newPos = p1(x) + inc;
        p1(x) = newPos;
        Screen('FillRect', window, color, CenterRectOnPointd(baseRect, p1(x), p1(y)));
        Screen('DrawLine', window, lineColor, posAnt, p1(y), newPos, p1(y),lineWidth);
       
    end

    function createNodeRight(label)
        
        color = getColor(label);
        
        posAnt = p3(x);
        newPos = p3(x) + inc;
        p3(x) = newPos;
        Screen('FillRect', window, color, CenterRectOnPointd(baseRect, p3(x), p3(y)));
        Screen('DrawLine', window, lineColor, posAnt, p3(y), newPos, p3(y),lineWidth);
        
    end

    function createNodeCenter(label)
        
        color = getColor(label);
        
        posAnt = p2(x);
        newPos = p2(x) + inc;
        p2(x) = newPos;
        Screen('FillRect', window, color, CenterRectOnPointd(baseRect, p2(x), p2(y)));
        Screen('DrawLine', window, lineColor, posAnt, p2(y), newPos, p2(y),lineWidth);
    end

    function [color] = getColor(label)
        switch(label)
            case 1
                color = [128/255 128/255 128/255];
            case 2
                color = [128/255 128/255 0];
            case 3
                color = [128/255 65/255 128/255];
            case 4
                color = [139/255 7/255 0/255];
            case 5
                color = [0 0 128/255];
            case 6
                color = [128/255 0 50/255];
            case 7
                color = [128/255 79/255 0];
            case 8
                color = [145/255 98/255 146/255];
        end
    end





end