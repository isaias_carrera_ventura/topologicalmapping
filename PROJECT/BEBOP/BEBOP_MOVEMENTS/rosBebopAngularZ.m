function rosBebopAngularZ(paramMovementRaw)
%ROSBEBOPTURNLEFT Summary of this function goes here
%   Detailed explanation goes here
chatpub = rospublisher('/cmd_vel','geometry_msgs/Twist');
pause(1);

twist = rosmessage(chatpub);

twist.Linear.X = 0;
twist.Linear.Y = 0;
twist.Linear.Z = 0;

twist.Angular.X = 0;
twist.Angular.Y = 0;
twist.Angular.Z = paramMovementRaw;

send(chatpub,twist);


end

