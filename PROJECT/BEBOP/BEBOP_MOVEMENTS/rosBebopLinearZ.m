function rosBebopLinearZ( paramMovementRaw )

chatpub = rospublisher('/cmd_vel','geometry_msgs/Twist');
pause(1);

twist = rosmessage(chatpub);

twist.Linear.X = 0;
twist.Linear.Y = 0;
twist.Linear.Z = paramMovementRaw;

twist.Angular.X = 0;
twist.Angular.Y = 0;
twist.Angular.Z = 0;

send(chatpub,twist);

end

