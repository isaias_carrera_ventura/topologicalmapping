function [evalResult,labelMax] = evalLabelBySection(posinit,finalpos,height,matrix )

bOcurrencies = [0,0,0,0,0,0,0,0];

for i = 1:height
    for j = posinit:finalpos
        bOcurrencies(matrix(i,j)) = bOcurrencies(matrix(i,j)) + 1;
    end
end


maxElement = bOcurrencies(1);
labelMax = 0;
for i = 1:numel(bOcurrencies)
    if(bOcurrencies(i) > maxElement)
        maxElement = bOcurrencies(i);
        labelMax = i;
    end
end

evalResult = bOcurrencies;


