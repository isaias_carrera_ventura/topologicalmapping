function [matrixSequence,row,col] = segmentationSequence()


fid = fopen('preds_.txt','r');
formatSpec = '%d,%d,%d';
sizeA = [3 inf];
A = fscanf(fid,formatSpec,sizeA);
%Define new matrix to process easier
[row, col] = size(A);
aux = row;
row = col;
col = aux;
matrixSequence = [row col];
for i=1:row
    for j=1:col
        matrixSequence(i,j) = A(j,i);
    end
end



end