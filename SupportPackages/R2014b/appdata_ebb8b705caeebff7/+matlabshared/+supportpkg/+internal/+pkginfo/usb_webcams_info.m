function supportpkg = usb_webcams_info()
%USB_WEBCAMS_INFO Return MATLAB Support Package for USB Webcams information.

%   Copyright 2016 The MathWorks, Inc.

supportpkg = hwconnectinstaller.SupportPackage();
supportpkg.Name          = 'USB Webcams';
supportpkg.Version       = '14.2.2';
supportpkg.Platform      = 'PCWIN,PCWIN64,MACI64,GLNXA64';
supportpkg.Visible       = '1';
supportpkg.FwUpdate      = '';
supportpkg.Url           = 'http://www.mathworks.com';
supportpkg.DownloadUrl   = '';
supportpkg.LicenseUrl    = '';
supportpkg.BaseProduct   = 'MATLAB';
supportpkg.AllowDownloadWithoutInstall = true;
supportpkg.FullName      = 'MATLAB Support Package for USB Webcams';
supportpkg.DisplayName      = 'USB Webcams';
supportpkg.SupportCategory      = 'hardware';
supportpkg.CustomLicense = '';
supportpkg.CustomLicenseNotes = '';
supportpkg.ShowSPLicense = true;
supportpkg.Folder        = 'webcam';
supportpkg.Release       = '(R2014b)';
supportpkg.DownloadDir   = '/Users/isaiasr/Documents/MATLAB/SupportPackages/R2014b/downloads/usbwebcams_download';
supportpkg.InstallDir    = '/Users/isaiasr/Documents/MATLAB/SupportPackages/R2014b';
supportpkg.IsDownloaded  = 0;
supportpkg.IsInstalled   = 1;
supportpkg.RootDir       = '/Users/isaiasr/Documents/MATLAB/SupportPackages/R2014b/usbwebcams/toolbox/matlab/webcam/supportpackages';
supportpkg.DemoXml       = 'webcamexamples/demos.xml';
supportpkg.ExtraInfoCheckBoxDescription       = '';
supportpkg.ExtraInfoCheckBoxCmd       = '';
supportpkg.FwUpdate      = '';
supportpkg.PreUninstallCmd      = '';
supportpkg.InfoUrl      = '';
supportpkg.BaseCode     = 'USBWEBCAM';
supportpkg.SupportTypeQualifier      = 'Standard';
supportpkg.CustomMWLicenseFiles      = '';
supportpkg.InstalledDate      = '26-Aug-2016 12:28:03';
supportpkg.InfoText      = 'Acquire+images+and+video+from+UVC+compliant+webcams.';
supportpkg.Path{1}      = '/Users/isaiasr/Documents/MATLAB/SupportPackages/R2014b/usbwebcams/toolbox/matlab/webcam/supportpackages';
supportpkg.Path{2}      = '/Users/isaiasr/Documents/MATLAB/SupportPackages/R2014b/usbwebcams/toolbox/matlab/webcam/supportpackages/webcamexamples';

% Third party software information
