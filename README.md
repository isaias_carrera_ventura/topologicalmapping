# Mapeo topológico

En este documento se presenta la forma de compilar y ejecutar el código realizado por Isaias Carrera Ventura como parte del servicio social que prestó en el Instituo Nacional de Astrofísica , Óptica y Electrónica.

Para la realización del proyecto se utilizaron las siguientes herramientas.

  - [Matlab 2016Rb](https://www.mathworks.com/products/matlab.html/)
    - [JGMT graphical models toolbox](http://users.cecs.anu.edu.au/~jdomke/JGMT/)
    - [Standford background dataset](http://dags.stanford.edu/projects/scenedataset.html)
    - [Piotr Dollar’s toolbox](http://pdollar.github.io/toolbox/)
    - [Image processing toolbox](https://www.mathworks.com/products/image.html?s_tid=srchtitle)
    - [Robotics system toolbox](https://www.mathworks.com/products/robotics.html)
   - [FFmpeg](https://ffmpeg.org/)
   - [Bebop drone SDK](http://bebop-autonomy.readthedocs.io/en/latest/#)
   - [ROS INDIGO](http://wiki.ros.org/indigo)

# Control del bebop drone usando MATLAB

Los scripts para poder controlar el drone bebop, están ubicados en:
```sh
.../TopologicalMapping/PROJECT/BEBOP/ 
.../TopologicalMapping/PROJECT/BEBOP/BEBOP_MOVEMENTS/ 
```

Para poder usar el dron con matlab es necesario tener instalado lo siguiente:

- MATLAB 2016Rb
- Robotics system toolbox (MATLAB)
- Bebop drone SDK
- ROS INDIGO

### Uso 

Para poder usar los scripts es necesario tener el ROS Master inicializado, así como el driver del bebop drone.
```sh
$ roscore 
$ roslaunch bebop_driver bebop_node.launch
```

En la ruta:
```sh
../TopologicalMapping/PROJECT/BEBOP/ 
```
Se encuentran el script para inicializar ROS  en Matlab.

En Matlab
```sh
>> rosBebopInit
```

Inicializará ROS en Matlab dando una lista de los 'topics' disponibles para publicar.

Una vez que se hayan terminado de usar los scripts es necesario cerrar ROS en Matlab, para eso se corre en Matlab:
```sh
>> rosBebopShutdown
```

Los scripts siguientes fueron modulados para que tuvieran la similitud a la documentación del [Driver del bebop](http://bebop-autonomy.readthedocs.io/en/latest/piloting.html)

Se describen como sigue

#### Despegue del vehículo
```sh
>> rosBebopTakeOff
```

#### Aterrizaje del vehículo
```sh
>> rosBebopLanding
```
#### Piloteo

##### Move Atras / Mover Adelante
- IncrementoDeAvance > 0  (Adelante)
- IncrementoDeAvance < 0  (Atrás)
```sh
>> rosBebopLinearX(incrementoDeAvance)
```
##### Mover Izquierda / Mover Derecha
- IncrementoDeAvance > 0  (Izquierda)
- IncrementoDeAvance < 0  (Derecha)
```sh
>> rosBebopLinearY(incrementoDeAvance)
```
##### Arriba / Abajo
- IncrementoDeAvance > 0  (Arriba)
- IncrementoDeAvance < 0  (Abajo)
```sh
>> rosBebopLinearZ(incrementoDeAvance)
```
##### Rotar Izquierda / Rotar Derecha
- IncrementoDeAvance > 0  (Izquierda)
- IncrementoDeAvance < 0  (Derecha)
```sh
>> rosBebopAngularZ(incrementoDeAvance)
```

# Mapeo topológico

El código fuente que se encarga de realizar lo propuesto está contenido en ```../TopologicalMapping/PROJECT/Histogram/ ```
### Segmentación de imagen

  - [JGMT graphical models toolbox](http://users.cecs.anu.edu.au/~jdomke/JGMT/) provee de las instrucciones detalladas para correr y poder integrar el método de entrenamiento y segmentación de la imagen usando matlab.
 
>  Nota. El proyecto tiene integrado todos los toolboxes necesarios para poder ser ejecutado, de lo contrario , seguir el tutorial de JGMT.

#### Uso

##### Video

El código está diseñado para poder ser ejecutado usando un video u obteniendo la imagen directamente del bebop, el video tendrá que ser almacenado en la computadora en formato MPEG, la herramienta [FFMPEG](https://ffmpeg.org/) es usada para poder exportar el video de MP4 al formato MPEG.
El video será de 320x240 en resolución, siendo esa resolución la mejor para el algoritmo.

```sh 
$ ffmpeg -i ../RutaVideo/NombreVideo.mp4 -s 320x240 ../RUTA/NombreVideoNuevoRedimensionado.mpeg
```
Posteriormente una vez redimensionado el video, copiar y pegar la ruta en el archivo`../TopologicalMapping/PROJECT/Histogram/camBebopDriver.m`

```
vid_read  = VideoReader('RUTA VIDEO REDIM');
```
##### Video de Bebop Drone

Para obtener la imagen del bebop, es necesario inicializar ROS  en Matlab, posteriormente es necesario suscribirte al topic `/image_raw` después obtener la imagen y procesarla en el método.

~~~~ 
s = rossubscriber('/image_raw');
data = receive(s);
img = readImage(data);
im0 = double(img)/255;
im0 = imresize(im0,0.5);
~~~~

##### Creación del mapa / KeyFrames 
`../TopologicalMapping/PROJECT/Histogram/camBebopDriver.m` es el archivo el cual contiene el código necesario para la creación y generación del mapa.

El procesamiento del video, crea el mapa en un archivo de texto, el cual contiene el promedio de los histogramas `preds_.txt` , al igual genera los keyframes de los nodos en una carpeta que contiene las imagenes `TopologicalMapping/PROJECT/imageVideo/ `  y un archivo de texto  `imageNameArray.txt` indicando la ruta de cada una de las imagenes para su posterior uso en la localización.

##### Localización
`../TopologicalMapping/PROJECT/Histogram/relocalization.m` esel archivo el cual contiene el código necesario para la localización.
 
El video o la obtención de la imagen del dron, será de igual forma utilizado.

> Ver punto del video.