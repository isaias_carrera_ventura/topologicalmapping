function eval_image

clf

p = importdata('p.mat');
nvals  = 8;
rho= .5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
loss_spec = 'trunc_cl_trwpll_5';
crf_type  = 'linear_linear';
edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};
feat_params = {{'patches',0},{'position',1},{'fourier',1},{'hog',8}};

% colormap-- taken from http://dags.stanford.edu/projects/scenedataset.html
cmap = [.5  .5  .5;
        .5  .5   0;
        .5  .25 .5;
        0   .5   0;
        0    0   .5;
        .5   0   0;
        .5  .31  0;
        1   .5   0];
    

img_read = imread('/home/isaiasr/Documents/MATLAB/img.png');
im0 = double(img_read)/255;
feats = featurize_im(im0,feat_params);
im= imresize(im0,.2,'bilinear');
feats = imresize(feats,.2,'bilinear');
[ly lx lz] = size(feats);
feats = reshape(feats,ly*lx,lz);
[ly lx lz] = size(im);
model = gridmodel(ly,lx,nvals);
efeats = edgeify_im(im,edge_params,model.pairs,model.pairtype);

% do inference
[b_i b_ij] = eval_crf(p,feats,efeats,model,loss_spec,crf_type,rho);

% interpolate beliefs to original image, get predictions
[ly0 lx0 lz] = size(im0);
b_i = reshape(b_i',[ly lx nvals]);
b_i = imresize(b_i,[ly0 lx0],'bilinear');

[~,x_pred] = max(b_i,[],3);

%**********************************
%preds_aux = miximshow(x_pred,nvals);
%imshow(preds_aux);
%whos x_pred
%***********************************

im_gray = repmat(rgb2gray(im0),[1 1 3]);
colormap(cmap)
preds = miximshow(b_i,nvals);

im_mix = .25*im_gray + .75*preds;
imshow(im_mix)
colorbar('Location','South','XTickLabel',...
{'sky','tree','road','grass','water','bldg','mntn','fg obj'});

%GET PARAMS
[height,width] = size(x_pred);
%PARTITION INTO 3 PIECES
partition = int16(width)/3;
partition = partition - 1;
%EVAL MATRIX{
[eval1,label1] = eval_labelsbyseccion(1,partition,height,x_pred);
[eval2,label2] = eval_labelsbyseccion(partition,partition*2,height,x_pred);
[eval3,label3] = eval_labelsbyseccion(partition*2,partition*3,height,x_pred);

fprintf('%d%d%d',label1,label2,label3);



end

